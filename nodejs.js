const { response } = require('express');
const https = require('https');

URL = 'https://dog.ceo/api/breeds/list/all';

const callApiUsingHttp = (callback) => {
    https.get(URL, (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
            data += chunk;
        })

        resp.on('end', () => {
            return callback(data);
        })
    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}

module.exports.callApi = callApiUsingHttp;